jQuery Plugin that no one should EVER use. Call it on an element, give it a target color, an attribute to change, and a time and it'll change the specified attribute to the target color, but it'll hit all the colors between the current color and the target.

Example usage:
    $('#some-text').shiftColor('blue', 'color', 2000); // blue, text-color, 2 seconds
    $('#some-div').shiftColor('#ff0000', 'background-color', 1000); // red, background-color, 1 second
    $('#some-div').shiftColor('rgb(0,255,0)', 'border-color', 500); // green, border-color, 1/2 second
    
v0.0.1
    + Basic Functionality